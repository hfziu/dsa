#pragma once

template<typename T>
Rank Vector<T>::search(T const& e) const {
	return (_size <= 0) ? -1 : search(e, 0, _size);
}

template<typename T>
Rank Vector<T>::search(T const& e, Rank lo, Rank hi) const {
	return binSearch(_elem, e, lo, hi); // binSearch() for array
}

template<typename T>
static Rank binSearch(T* A, T const& e, Rank lo, Rank hi) {
	while (lo < hi) {
		Rank mi = (lo + hi) >> 1;
		(e < A[mi]) ? hi = mi : lo = mi + 1;
	}
	return --lo;
}