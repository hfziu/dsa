#include<iostream>
#include<string>
#include<cstdlib>
#include "Vector.h"

using namespace std;

int main() {
	int *A = new int[30];
	string str_creating = "Creating array A...";
	string str_created = "Array A created.";
	string str_creating_vector = "Creating vector V...";
	string str_created_vector = "Created vector V";
	cout << str_creating << endl;
	for (int i = 0; i < 30; i++) {
		A[i] = i / 3;
		//std::cout << A[i] << " ";
	}
	cout << str_created << endl;
	cout << str_creating_vector << endl;
	Vector<int> V(A, 30);
	cout << str_created_vector << endl;
	cout << "V.size(): " << V.size() << endl;
	cout << "V.disordered(): " << V.disordered() << endl;
	cout << "V.find(5): " << V.find(5) << endl;
	//print_elem(V); cout << endl;
	V.uniquify();
	cout << "V.insert(30): " << V.insert(30) << endl;
	cout << "V.insert(10, 50): " << V.insert(10, 30) << endl;
	cout << "V.size(): " << V.size() << endl;
	//print_elem(V);
	cout << endl;
	system("PAUSE");
	return 0;
}