#pragma once

#include "Vector_copyFrom.h"
#include "Vector_expand.h"
#include "Vector_shrink.h"

#include "Vector_disordered.h"
#include "Vector_find.h"
#include "Vector_search.h"

#include "Vector_operators.h"

#include "Vector_remove.h"
#include "Vector_insert.h"

#include "Vector_uniquify.h"

//#include "Vector_traverse.h"