#pragma once

template <typename T>
void Vector<T>::traverse(void(*visit)(T&)) {
	for (int i = 0; i < _size; i++) {
		visit(_elem[i]);
	}
}

template <typename T> template <typename VST>
void Vector<T>::traverse(VST& visit) {
	for (int i = 0; i < _size; i++) {
		visit(_elem[i]);
	}
}

//template <typename T>
//struct Print {
//	virtual void operator()(T& e) {
//		std::cout << e << " ";
//	}
//};
//
//template <typename T>
//void print_elem(Vector<T> & V) {
//	V.traverse(Print<T>());
//}