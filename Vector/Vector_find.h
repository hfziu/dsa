#pragma once

template<typename T>
Rank Vector<T>::find(T const& e) const {
	return find(e, 0, _size);
}

template<typename T>
Rank Vector<T>::find(T const& e, Rank lo, Rank hi) const {
	while ((lo < hi--) && (e != _elem[hi]));
	return hi;
}