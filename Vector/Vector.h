#pragma once

typedef int Rank;
#define DEFAULT_CAPACITY 3

template <typename T>
class Vector {
protected:
	Rank _size; int _capacity; T* _elem; // Size, Capacity and Elements
	void copyFrom(T const* A, Rank lo, Rank hi);
	void expand();
	void shrink();
	Rank max(Rank lo, Rank hi);
public:
	// Constructors
	Vector(int c = DEFAULT_CAPACITY, int s = 0, T v = 0) {
		_elem = new T[_capacity = c];
		for (_size = 0; _size < s; _elem[_size++] = v);
	}
	Vector(T const* A, Rank n) {
		copyFrom(A, 0, n);
	}
	Vector(T const* A, Rank lo, Rank hi) {
		copyFrom(A, lo, hi);
	}
	Vector(Vector<T> const& V) {
		copyFrom(V._elem, 0, V._size);
	}
	Vector(Vector<T> const& V, Rank lo, Rank hi) {
		copyFrom(V._elem, lo, hi);
	}
	// Destructor
	~Vector() {
		delete[] _elem;
	}

	// Read only interfaces
	Rank size() const { // Size
		return _size;
	}
	bool empty() const { // Is empty
		return !_size;
	}
	int disordered() const; // Return number of inversions

	// Find and search
	Rank find(T const& e) const;
	Rank find(T const& e, Rank lo, Rank hi) const;
	Rank search(T const& e) const;
	Rank search(T const& e, Rank lo, Rank hi) const;

	// Writable interfaces
	T& operator[] (Rank r) const;
	Vector<T> & operator= (Vector<T> const&);

	T remove(Rank r);
	int remove(Rank lo, Rank hi);
	Rank insert(T const& e);
	Rank insert(Rank r, T const& e);

	// Deduplicate
	int uniquify(); // deduplicate for ordered vector

	// Traverse
	void traverse(void(*) (T&));
	template <typename VST> void traverse(VST&);
};

#include "Vector_Implementation.h"